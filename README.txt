
#
# wget -qO- https://gitlab.com/i3-public/i3-web-page/-/raw/master/README.txt | bash -s i3-web-page

if [ ! -d /docker/$1 ]; then
    git clone https://gitlab.com/i3-public/i3-web-page.git /docker/$1
fi

wget -qO- https://gitlab.com/i3-public/conf/-/raw/main/nginx-php/README.txt | bash -s 7.4 $1 80 -- '-v /docker/'$1':/var/www --hostname '$1' -p 443:443'

wget -qO- https://gitlab.com/i3-public/net-usage/-/raw/master/README.txt | bash -s 80 skipserver

 
