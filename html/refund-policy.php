<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="canonical" href="<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/policy.php"/>
    <!-- Title -->
    <title>IPTV TREE - IPTV PROVIDER</title>

    <!-- Favicon -->
    <link rel="icon" href="./img/core-img/">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <!-- Preloader -->
    <!-- <div id="preloader">
        <div class="loader"></div>
    </div> -->
    <!-- /Preloader -->

    <header class="header-area">
        <!-- Top Header Area Start -->
        <div class="top-header-area">
            <div class="container">
                <div class="row">

                    <div class="col-6">
                        <div class="top-header-content">
                            <a href="/terms.php" target="_blank"><i class="fa fa-bell" aria-hidden="true"></i> <span>Terms</span></a> 
                            <a href="/refund-policy.php" target="_blank"><i class="fa fa-cc-paypal" aria-hidden="true"></i> <span>Refund Policy</span></a>
                            &nbsp &nbsp
                            <a href="/faq.php" target="_blank"><i class="fa fa-comments" aria-hidden="true"></i> <span>FAQ</span></a>
                            &nbsp &nbsp
                            <a href="https://wa.me/message/HST6SMCT67IOL1" target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i> <span>WhatsApp</span></a> &nbsp &nbsp
                            <a href="https://t.me/iptvtree" target="_blank"><i class="fa fa-telegram" aria-hidden="true"></i> <span>Telegram</span></a>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="top-header-content">
                            <!-- Login -->
                            <a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/login" target="_blank"><i class="fa fa-user" aria-hidden="true"></i> <span>Login</span></a>
                            <a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/register" target="_blank"><i class="fa fa-lock" aria-hidden="true"></i> <span>Register</span></a>&nbsp &nbsp
                            <a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/support" target="_blank"><i class="fa fa-comments" aria-hidden="true"></i> <span>Support</span></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Top Header Area End -->

        <!-- Main Header Start -->
        <div class="main-header-area">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Classy Menu -->
                    <nav class="classy-navbar justify-content-between" id="hamiNav">

                        <!-- Logo -->
                        <a class="nav-brand" href="index.php"><img src="/img/core-img/logo.png" alt=""></a>

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">
                            <!-- Menu Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>
                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul id="nav">
                                    <li class="active"><a href="/index.php">Home</a></li>
                                    <li><a href="/reseller.php">Reseller</a></li>
                                    <li><a href="/restream.php">Restream</a>
                                        <!-- <ul class="dropdown">
                                            <li><a href="./index.php">- Home</a></li>
                                            <li><a href="./hosting.php">- Hosting</a></li>
                                            <li><a href="./about.php">- About</a></li>
                                            <li><a href="./blog.php">- Blog</a></li>
                                            <li><a href="./single-blog.php">- Blog Details</a></li>
                                            <li><a href="./404.php">- 404</a></li>
                                            <li><a href="./coming-soon.php">- Coming Soon</a></li>
                                            <li><a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/">- Dropdown</a>
                                                <ul class="dropdown">
                                                    <li><a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/">- Dropdown Item</a></li>
                                                    <li><a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/">- Dropdown Item</a></li>
                                                    <li><a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/">- Dropdown Item</a></li>
                                                    <li><a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/">- Dropdown Item</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>-->
                                    <li><a href="/multiroom.php">Multiroom</a></li>
                                    <li><a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>//service/line/new/101" target="_blank">24Hour</a></li>
                                    <li><a href="/faq.php">Tutorial</a></li>
                                    
                                </ul>

                                <!-- Live Chat -->
                                <!--<div class="live-chat-btn ml-5 mt-4 mt-lg-0 ml-md-4">
                                    <a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>//support/new" target="_blank" class="btn hami-btn live--chat--btn"><i class="fa fa-comments" aria-hidden="true"></i> Support</a>
                                </div>-->
                            </div>
                            <!-- Nav End -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header Area End -->

    <!-- Video Area Start -->
    <div class="hami--video--area section-padding-100-0">
        <div class="container">
            <div class="row">
                <!-- Section Heading -->
                <div class="col-12">
                    <div class="section-heading text-center">
                        <h2>Refund Policy</h2>
                        <h3 align="left">IPTV TREE Refund Policy</h3>
    <p align="left">Please read the following carefully, then send us a refund request by creating a ticket in the support section.</p>

<h3 align="left">Requests for Refund must be clear and acceptable:</h3>
    <p align="left">√ Before you buy, be sure to request a trial account. We have set up a trial account so that you can test it and if you are not satisfied, do not buy a long-term service.</p>
    
    <p align="left">√ If your internet speed is slow, you will not be able to see and use our iptvtree services and then request a refund.</p>

    <p align="left">√ The big iptvtree team is always ready to respond 24 hours a day, seven days a week, and if you can't set up the iptvtree service on the devices you have for any reason, it won't include refunds. Please see and learn the necessary tutorials on setting up iptv.</p>

    <p align="left">√ If you made a mistake in choosing the service and completed the payment, you can find a ticket in the support section entitled; Create then our support team will review it and accept and do the refund. ; Payment error; This means that you want to buy a 3-month service and have mistakenly chosen and paid for a year or less.</p>

    <p align="left">√ If your account expires 7 days later, the refund request will not be accepted.</p>

                    </div>
                </div>
            </div>

    <!-- Footer Area Start -->
    <footer class="footer-area section-padding-80-0">

        <!-- Bottom Footer Area -->
        <div class="bottom-footer-area bg-gray">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-md-6">
                        <!-- Copywrite Text -->
                        <div class="copywrite-text">
                            <p><!-- Link back to Colorlib can't be removed. Website is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This Website is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="/" target="_blank">Tomas Arana</a>
<!-- Link back to Colorlib can't be removed. Website is licensed under CC BY 3.0. -->
</p>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <!-- Payment Methods -->
                        <div class="payments-methods d-flex align-items-center">
                            <p>Payments We Accept</p>
                            <i class="fa fa-cc-visa" aria-hidden="true"></i>
                            <i class="fa fa-cc-mastercard" aria-hidden="true"></i>
                            <i class="fa fa-cc-discover" aria-hidden="true"></i>
                            <i class="fa fa-cc-amex" aria-hidden="true"></i>
                            <i class="fa fa-cc-paypal" aria-hidden="true"></i>
                            <i class="fa fa-cc-stripe" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Start Cookie Script-->
        <script type="text/javascript" charset="UTF-8" src="http://chs03.cookie-script.com/s/ee4d1f3f84eb09c7a0e378ef218e103d.js"></script>
        <!--End Cookie Script-->
    </footer>
    <!-- Footer Area End -->

    <!-- **** All JS Files ***** -->
    <!-- jQuery 2.2.4 -->
    <script src="js/jquery.min.js"></script>
    <!-- Popper -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- All Plugins -->
    <script src="js/hami.bundle.js"></script>
    <!-- Faq -->
    <script src="js/faq.tree.js"></script>
    <!-- Active -->
    <script src="js/default-assets/active.js"></script>

</body>

</html>