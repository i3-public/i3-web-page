<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta name="description" content="">
    <meta name="keywords" content="buy iptv, buy restream, iptv restream, iptv channels, iptv subscribe, iptv subscription, german iptv, Kaufen IPTV, UK iptv,">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="canonical" href="https://<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/policy.php"/>
    <!-- Title -->
    <title>IPTVTREE - IPTV PROVIDER</title>

    <!-- Favicon -->
    <link rel="icon" href="./img/core-img/">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <!-- Preloader -->
    <!-- <div id="preloader">
        <div class="loader"></div>
    </div> -->
    <!-- /Preloader -->

    <!-- Header Area Start -->
    <header class="header-area">
        <!-- Top Header Area Start -->
        <div class="top-header-area">
            <div class="container">
                <div class="row">

                    <div class="col-6">
                        <div class="top-header-content">
                            <a href="/terms.php" target="_blank"><i class="fa fa-bell" aria-hidden="true"></i> <span>Terms</span></a> 
                            <a href="/refund-policy.php" target="_blank"><i class="fa fa-cc-paypal" aria-hidden="true"></i> <span>Refund Policy</span></a>
                            &nbsp&nbsp
                            <a href="/faq.php" target="_blank"><i class="fa fa-comments" aria-hidden="true"></i> <span>FAQ</span></a>
                            &nbsp &nbsp
                            <a href="https://wa.me/message/HST6SMCT67IOL1" target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i> <span>WhatsApp</span></a> &nbsp &nbsp
                            <a href="https://t.me/iptvtree" target="_blank"><i class="fa fa-telegram" aria-hidden="true"></i> <span>Telegram</span></a>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="top-header-content">
                            <!-- Login -->
                            <a href="https://buy.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/login" target="_blank"><i class="fa fa-user" aria-hidden="true"></i> <span>Login</span></a>
                            <a href="https://buy.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/register" target="_blank"><i class="fa fa-lock" aria-hidden="true"></i> <span>Register</span></a>&nbsp &nbsp
                            <a href="https://buy.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/support" target="_blank"><i class="fa fa-comments" aria-hidden="true"></i> <span>Support</span></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Top Header Area End -->

        <!-- Main Header Start -->
        <div class="main-header-area">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Classy Menu -->
                    <nav class="classy-navbar justify-content-between" id="hamiNav">

                        <!-- Logo -->
                        <a class="nav-brand" href="index.php"><img src="/img/core-img/logo.png" alt=""></a>

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">
                            <!-- Menu Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>
                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul id="nav">
                                    <li class="active"><a href="/index.php">Home</a></li>
                                    <li><a href="/reseller.php">Reseller</a></li>
                                    <li><a href="/restream.php">Restream</a>
                                    <li><a href="/multiroom.php">Multiroom</a></li>
                                    <li><a href="https://buy.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/service/line/new/101" target="_blank">24Hour</a></li>
                                    <li><a href="https://www.youtube.com/channel/UCJGO-yy54MwvfEVzqWnLXQw" target="_blank">Tutorial</a></li>
                                    
                                </ul>
                            </div>
                            <!-- Nav End -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header Area End -->

    <!-- Video Area Start -->
    <div class="hami--video--area section-padding-100-0">
        <div class="container">
            <div class="row">
                <!-- Section Heading -->
                <div class="col-12">
                    <div class="section-heading text-center">
                        <h1>Terms And Conditions</h1>
                        <h2 align="left">IPTVTREE Terms and Conditions</h2>
    <p align="left">IPTV TREE provides an online video streaming service which allows clients to browse distinctive offerings of live and on-demand programs. Consequently, by accessing or using any of the IPTV TREE services, you acknowledge and consent to these terms:</p>

<h3 align="left">Changes to the Terms and Conditions</h3>
    <p align="left">We may, at any time, and at our sole discretion, adjust these Terms and Conditions of Use, including our Privacy Policy, with or without notice to the Client. Any such alteration will be effective instantly upon open posting. Your proceeded use of our Service and this Site following any such alteration constitutes your acknowledgment of these adjusted Terms.</p>
    
<h3 align="left">Minimum age requirement</h3>
    <p align="left">In order to become a member and utilize the IPTV TREE services, you must be 18 years old or older.</p>

<h3 align="left">Usage and service terms<h3>
    <p align="left">“You may not either directly or through the use of any device, software, internet site, web-based service, or other means, re-stream, distribute, broadcast or transmit the content.</p>

<h3 align="left">Third party purchases</h3>
    <p align="left">Your transactions and other dealings with third party vendors that are found on or through the service, including “click to purchase”, and other comparable programs, are solely between you and such dealer.</p>

<h3 align="left">Quality of streams</h3>
    <p align="left">A perfect viewing experience relies upon your network access and device capabilities. The elements will be based on your location, internet capacity, the quantity of devices connected to the same network, the content you have chosen, and the configuration of the device you are using.Subsequently, IPTV TREE can’t make any guarantees about the content in these regards. Please note sharing a subscription will result in permanent suspension or device ban.</p>

<h3 align="left">Used balance</h3>
    <p align="left">Please note that all the subscriptions made and credits used are final and non-refundable under any circumstances.</p>

<h3 align="left">Unsupported regions<h3>
    <p align="left">“Our services are unavailable in the following countries: √ Iran</p>

<h3 align="left">Accuracy of information</h3>
    <p align="left">All the data you submit to our database must be accurate and updated. Please keep your passwords safe. You won’t need to uncover it to any IPTV TREE agents. You are responsible for all utilization of your account.</p>

<h3 align="left">Compatibility<h3>
    <p align="left">In order to access the services, you must use devices that meet the system and compatibility prerequisites that we establish in our Help center.</p>

<h3 align="left">Internet service and data usage</h3>
    <p align="left">You are in charge of any expenses related to your network access used to get to our services.</p>

<h3 align="left">Sharing a subscription<h3>
    <p align="left">Sharing a subscription is not permitted. You can only have one active stream open at any given time. However, by purchasing extra connections, you can watch on multiple devices at the same time.</p>

<h3 align="left">Purchase details<h3>
    <p align="left">In order to make a purchase, you will need to follow the ordering procedures described via the service. Pricing details for products and the procedures for payment and delivery are displayed via the service, and are subject to change without notice.</p>

<h3 align="left">Suspension and downtime<h3>
    <p align="left">In extension of our rights to end or suspend your access delineated above, you acknowledge that: your access and usage of the services might be suspended for the length of any sudden or unscheduled downtime or unavailability of any portion or all of the services for any reason.
    All correspondences and notice to be made or offered in understanding with this Agreement ought to be in the English language. We maintain all authority to instantly end or limit your use of the services or access to content at any time, without notice or liability, if IPTV TREE determines in its sole discretion that you have breached these Terms, disregarded any law, rule, or regulation.
    The IPTV TREE logo, and other IPTV TREE marks, graphics, scripts, are trademarks of IPTV TREE. None of the IPTV TREE trademarks may be copied, downloaded, or otherwise exploited.</p>

                    </div>
                </div>
            </div>

            

    <!-- **** All JS Files ***** -->
    <!-- jQuery 2.2.4 -->
    <script src="js/jquery.min.js"></script>
    <!-- Popper -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- All Plugins -->
    <script src="js/hami.bundle.js"></script>
    <!-- Active -->
    <script src="js/default-assets/active.js"></script>

</body>

</html>