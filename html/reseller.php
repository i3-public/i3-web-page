
<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta name="description" content="We are owner IPTV server | Provide IPTV Reseller Panel | Dedicated Dashboard panel | We have more than 12,000 channels and more Than 30,000 VOD and series. 200 Credit 200€ | 400 Credit 4500€ | 800 Credit 1000€ | 1500 Credit 2000€">
    <meta name="keywords" content="buy iptv, iptv reseller, reseller subscribe , iptv panel, iptv dashboard, reseller subscription, become  reseller, iptvtree reseller, channels, iptv channels, iptv subscribe, iptv subscription, subreseller, subreseller iptv">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="canonical" href="<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/reseller.php"/>

    <!-- Title -->
    <title>IPTVTREE - IPTV Reseller PROVIDER</title>

    <!-- Favicon -->
    <link rel="icon" href="/img/core-img/">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">

</head>

<body>

    <!-- Header Area Start -->
    <header class="header-area">
        <!-- Top Header Area Start -->
        <div class="top-header-area">
            <div class="container">
                <div class="row">

                    <div class="col-6">
                        <div class="top-header-content">
                            <a href="/terms.php" target="_blank"><i class="fa fa-bell" aria-hidden="true"></i> <span>Terms</span></a> 
                            <a href="/refund-policy.php" target="_blank"><i class="fa fa-cc-paypal" aria-hidden="true"></i> <span>Refund Policy</span></a>
                            &nbsp&nbsp
                            <a href="/faq.php" target="_blank"><i class="fa fa-comments" aria-hidden="true"></i> <span>FAQ</span></a>
                            &nbsp &nbsp
                            <a href="https://wa.me/message/HST6SMCT67IOL1" target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i> <span>WhatsApp</span></a> &nbsp &nbsp
                            <a href="https://t.me/iptvtree" target="_blank"><i class="fa fa-telegram" aria-hidden="true"></i> <span>Telegram</span></a>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="top-header-content">
                            <!-- Login -->
                            <a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/login" target="_blank"><i class="fa fa-user" aria-hidden="true"></i> <span>Login</span></a>
                            <a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/register" target="_blank"><i class="fa fa-lock" aria-hidden="true"></i> <span>Register</span></a>&nbsp &nbsp
                            <a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/support" target="_blank"><i class="fa fa-comments" aria-hidden="true"></i> <span>Support</span></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Top Header Area End -->

        <!-- Main Header Start -->
        <div class="main-header-area">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Classy Menu -->
                    <nav class="classy-navbar justify-content-between" id="hamiNav">

                        <!-- Logo -->
                        <a class="nav-brand" href="index.php"><img src="/img/core-img/logo.png" alt=""></a>

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">
                            <!-- Menu Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>
                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul id="nav">
                                    <li class="active"><a href="/index.php">Home</a></li>
                                    <li><a href="/reseller.php">Reseller</a></li>
                                    <li><a href="/restream.php">Restream</a>
                                    <li><a href="/multiroom.php">Multiroom</a></li>
                                    <li><a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/service/line/new/101" target="_blank">24Hour</a></li>
                                    <li><a href="https://www.youtube.com/channel/UCJGO-yy54MwvfEVzqWnLXQw" target="_blank">Tutorial</a></li>
                                    
                                </ul>
                            </div>
                            <!-- Nav End -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header Area End -->

    <!-- Breadcrumb Area Start -->
    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.php"><i class="icon_house_alt"></i> Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Reseller</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Area End -->

    <!-- Price Plan Area Start -->
    <section class="hami-price-plan-area mt-50">
        <div class="container">
            <div class="row">
                <!-- Section Heading -->
                <div class="col-12">
                    <div class="section-heading text-center">
                        <h1>Become an IPTV reseller</h1>
                        <h2>Start your own business right now and enjoy it.</h2>
                        <p>You can get into the iptv business world by buying a reseller panel.
                        In the dedicated iptvtree panel you will have different capabilities.
                        Some reseller panel capabilities:
                        ,Dedicated DNS selection
                        ,Select categories
                        ,Create a sub-reseller
                        ,Add credit to sub-reseller
                        ,Easy Buy Credit
                        ,Edit line
                        ,You can make 50 Test Line per day.
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="post-thumbnail mb-50">
                        <center><img src="/img/bg-img/18.jpg" alt="reseller"></center>
                    </div>
                </div>
            </div>


<!-- video Area Start -->
<video width="320" height="240" controls>
<source src="resellermedia.mp4" type="video/mp4">
    <source src="movie.ogg" type="video/ogg">
    Your browser does not support the video tag.
  </video>
<!-- video Area End -->

            <div class="text">
                <p>“IPTV RESELLER”</p>
                <p>“You can get into the iptv business world by buying a reseller panel.
                    In the dedicated iptvtree panel you will have different capabilities.”</p>
            </div>


            <div><h2 class="mb-4">Learn more about the IPTV Tree dedicated panel</h2>

                        <p>Customize your channels or better say the categories you want, and you can easily edit the user line each time the customer requests you to delete or add categories.
                        You can view MAC addresses or lines of different formats in the panel and then create lines.</p>

                        <ul>
                            <li><span><i class="fa fa-circle" aria-hidden="true"></i> you can help us selling IPTV,</span> The Aslo, having a new reseller panel. On reseller panel you can have your own IPTV clients, with all options, fully guaranteed privacy in your business. I mean, you can have your own DNS, and have your own list of clients, your own price list. if you are inrested you can see more details about the reseller service here.</li>

                            <li><span><i class="fa fa-circle" aria-hidden="true"></i> About DNS:</span> You can create your own domain name as well as submit it to your sub reseller and if your sub reseller requests you to change your domain name or DNS. You can easily do that for him.</li>

                            <li><span><i class="fa fa-circle" aria-hidden="true"></i> About Sub-Reseller Service:</span> To create each sub-reseller you have to pay 10 credits. That these 10 credits will be automatically deducted from your balance.
                            You can offer your sub-resellers different prices.
                            Note that the price list will not change at all. More to explain: You can't set a price list for your sub-reseller. Only when adding credit. For example, get 1 euro instead of 2 euro.</li>
                        </ul>

                        <p>Note that this panel will not expire and will not be lost if you do not use it for a long time.</p>
            </div>


<!-- ////////// -->
<div class="related-news-area section-padding-80-0">
                       <h2>Credit Cost For Reseller</h2>

                        <div class="row justify-content-between">
                            <!-- Single Related News Area -->
                            <div class="col-12 col-sm-6 col-lg-5">
                                <div class="single-related-news mb-80">
                                        <h2>1 connection</h2>
                        <!-- Description -->
                        <div class="cta-desc mb-50">
                            <h6><i class="icon_check"></i> 1 Month=5 Credit</h6>
                            <h6><i class="icon_check"></i> 3 Month=12 Credit</h6>
                            <h6><i class="icon_check"></i> 6 Month=20 Credit</h6>
                            <h6><i class="icon_check"></i> 12 Month=36 Credit</h6>
                        </div></a>
                                </div>
                            </div>
                            <!-- Single Related News Area -->
                            <div class="col-12 col-sm-6 col-lg-5">
                                <div class="single-related-news mb-80">
                                        <h2>2 connection (Multiroom)</h2>
                        <!-- Description -->
                        <div class="cta-desc mb-50">
                            <h6><i class="icon_check"></i> 1 Month=8 Credit</h6>
                            <h6><i class="icon_check"></i> 3 Month=18 Credit</h6>
                            <h6><i class="icon_check"></i> 6 Month=30 Credit</h6>
                            <h6><i class="icon_check"></i> 12 Month=50 Credit</h6>
                        </div></a>
                                </div>
                            </div>
                        </div>
                    </div>
<!-- ////////// -->
</br>
                    <!-- Button -->
                       <center><a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/service/line/new/101" target="_blank" class="btn hami-btn mb-450">Reseller Test</a></center> </br>

            <div class="row">
                <!-- Section Heading -->
                <div class="col-12">
                    <div class="section-heading text-center">
                        <h2>Choose Your Add credit</h2>
                        <p>You can easily top up your reseller account by selecting any of the following options.</p>
                    </div>
                </div>
            </div>


            <div class="row justify-content-center">

                <!-- Single Price Plan -->
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="single-price-plan mb-100">
                        <!-- Title -->
                        <div class="price-plan-title">
                            <h2>450</h2>
                            <h3>Credit</h3>
                        </div>
                        <!-- Value -->
                        <div class="price-plan-value">
                            <h2><span>€</span>400</h2>
                        </div>
                        <!-- Button -->
                        <a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/service/reseller/new/400" target="_blank" class="btn hami-btn w-100 mb-30">Add Credit</a>
                        
                        <!-- View All Feature Button -->
                    </div>
                </div>
                <!-- Single Price Plan -->
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="single-price-plan active mb-100">
                        <!-- Title -->
                        <div class="price-plan-title">
                            <h2>1000</h2>
                            <h3>Credit</h3>
                        </div>
                        <!-- Value -->
                        <div class="price-plan-value">
                            <h2><span>€</span>800</h2>
                        </div>
                        <!-- Button -->
                        <a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/service/reseller/new/800" target="_blank" class="btn hami-btn w-100 mb-30">Add Credit</a>
                        
                        <!-- View All Feature Button -->
                    </div>
                </div>

                <!-- Single Price Plan -->
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="single-price-plan active mb-100">
                        <div class="popular-tag">
                            <i class="icon_star"></i> Best Plan <i class="icon_star"></i>
                        </div>
                        <!-- Title -->
                        <div class="price-plan-title">
                            <h2>1400</h2>
                            <h3>Credit</h3>
                        </div>
                        <!-- Value -->
                        <div class="price-plan-value">
                            <h2><span>€</span>1000</h2>
                        </div>
                        <!-- Button -->
                        <a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/service/reseller/new/1000" target="_blank" class="btn hami-btn w-100 mb-30">Add Credit</a>
                        
                        <!-- View All Feature Button -->
                    </div>
                </div>

                <!-- Single Price Plan -->
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="single-price-plan mb-100">
                        <!-- Title -->
                        <div class="price-plan-title">
                            <h2>2000</h2>
                            <h3>Credit</h3>
                        </div>
                        <!-- Value -->
                        <div class="price-plan-value">
                            <h2><span>€</span>1500</h2>
                        </div>
                        <!-- Button -->
                        <a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/service/reseller/new/1500" target="_blank" class="btn hami-btn w-100 mb-30">Add Credit</a>
                        
                        <!-- View All Feature Button -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Price Plan Area End -->

    <!-- Call To Action Area Start -->
    <section class="hami-call-to-action">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-md-6">
                    <div class="cta-thumbnail pr-3 mb-100">
                        <img src="/img/bg-img/demopanel.png" alt="">
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="cta--content pl-3 mb-100">
                        <h2>Demo Reseller IPTV Panel</h2>
                        <p>To test the proprietary iptvtree panel much better and select and buy with a much better mindset. You can test the resellerpanel with the following specifications.
                        Keep in mind that you cannot create any account inside the demo panel.
                        Refer to the Trial section for a test account.</p>
                        <p color="blue"><b><a href="http://reseller.media" target="_blank" class="btn hami-btn mt-50">Click To Login Demo Panel</a></br></br>
                        Username : <a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="e682838b89a682838b89c885898b">[email&https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>160;protected]</a> </br>Password : demo123</p>
                        <!-- Button -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Call To Action Area End -->
    
    <!-- Support Area Start -->
    <section class="hami-support-area bg-White">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="support-text">
                        <h2>Need help? Call our award-winning support team 24/7: Chat online</h2>
                    </div>
                </div>
            </div>
        </div>

        <!-- Support Pattern -->
        <div class="support-pattern" style="background-image: url(/img/core-img/support-pattern.png);"></div>
    </section>
    <!-- Support Area End -->

    <!-- Call To Action Area Start -->
    <section class="hami-cta-area">
        <div class="container">
            <div class="cta-text">
                <h2>We Are Proud To Host More Than <span class="counter">800</span> Reseller and Over <span class="counter">50,000</span> Client</h2>
            </div>
        </div>
    </section>
    <!-- Call To Action Area End -->

    <!-- Footer Area Start -->
    <footer class="footer-area section-padding-80-0">

        <!-- Bottom Footer Area -->
        <div class="bottom-footer-area bg-gray">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-md-6">
                        <!-- Copywrite Text -->
                        <div class="copywrite-text">
                            <p><!-- Link back to Colorlib can't be removed. Website is licensed under CC BY 3.0. -->
Copyright &copy;<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script>document.write(new Date().getFullYear());</script> All rights reserved | This WebSite is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="/" target="_blank">Tomas Arana</a>
<!-- Link back to Colorlib can't be removed. Website is licensed under CC BY 3.0. -->
</p>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <!-- Payment Methods -->
                        <div class="payments-methods d-flex align-items-center">
                            <p>Payments We Accept</p>
                            <i class="fa fa-cc-visa" aria-hidden="true"></i>
                            <i class="fa fa-cc-mastercard" aria-hidden="true"></i>
                            <i class="fa fa-cc-discover" aria-hidden="true"></i>
                            <i class="fa fa-cc-amex" aria-hidden="true"></i>
                            <i class="fa fa-cc-paypal" aria-hidden="true"></i>
                            <i class="fa fa-cc-stripe" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Area End -->

    <!-- **** All JS Files ***** -->
    <!-- jQuery 2.2.4 -->
    <script src="js/jquery.min.js"></script>
    <!-- Popper -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- All Plugins -->
    <script src="js/hami.bundle.js"></script>
    <!-- Active -->
    <script src="js/default-assets/active.js"></script>

</body>

</html>