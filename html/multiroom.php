<!DOCTYPE html>
<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta name="description" content="We are owner IPTV server | Provide IPTV restream and IPTV Reseller Panel | We have more than 12,000 channels and more Than 10,000 VOD and series.1 Month €15 | 3 Month €25 | 3 Month €45 | 12 Month €75">
    <meta name="keywords" content="buy iptv, iptv reseller, iptv restream, iptv channels, iptv subscribe, iptv subscription, german iptv, uk iptv, sky sports, bein sports, bein movie, supersport, free iptv, iptv trial, iptv free trial, iptv local, local channel, local restream, iptvbox, magiptv, iptv apk, multiroom IPTV, dedicated iptv panel, iptv panel, iptv dashboard, 4K IPTV, FHD IPTV, UHD IPTV ">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="canonical" href="<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/multiroom.php"/>

    <!-- Title -->
    <title>IPTVTREE - IPTV PROVIDER</title>

    <!-- Favicon -->
    <link rel="icon" href="/img/core-img/">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <!-- Header Area Start -->
    <header class="header-area">
        <!-- Top Header Area Start -->
        <div class="top-header-area">
            <div class="container">
                <div class="row">

                    <div class="col-6">
                        <div class="top-header-content">
                            <a href="/terms.php" target="_blank"><i class="fa fa-bell" aria-hidden="true"></i> <span>Terms</span></a> 
                            <a href="/refund-policy.php" target="_blank"><i class="fa fa-cc-paypal" aria-hidden="true"></i> <span>Refund Policy</span></a>
                            &nbsp&nbsp
                            <a href="/faq.php" target="_blank"><i class="fa fa-comments" aria-hidden="true"></i> <span>FAQ</span></a>
                            &nbsp &nbsp
                            <a href="https://wa.me/message/HST6SMCT67IOL1" target="_blank"><i class="fa fa-whatsapp" aria-hidden="true"></i> <span>WhatsApp</span></a> &nbsp &nbsp
                            <a href="https://t.me/iptvtree" target="_blank"><i class="fa fa-telegram" aria-hidden="true"></i> <span>Telegram</span></a>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="top-header-content">
                            <!-- Login -->
                            <a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/login" target="_blank"><i class="fa fa-user" aria-hidden="true"></i> <span>Login</span></a>
                            <a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/register" target="_blank"><i class="fa fa-lock" aria-hidden="true"></i> <span>Register</span></a>&nbsp &nbsp
                            <a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/support" target="_blank"><i class="fa fa-comments" aria-hidden="true"></i> <span>Support</span></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Top Header Area End -->

        <!-- Main Header Start -->
        <div class="main-header-area">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Classy Menu -->
                    <nav class="classy-navbar justify-content-between" id="hamiNav">

                        <!-- Logo -->
                        <a class="nav-brand" href="index.php"><img src="/img/core-img/logo.png" alt=""></a>

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">
                            <!-- Menu Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>
                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul id="nav">
                                    <li class="active"><a href="/index.php">Home</a></li>
                                    <li><a href="/reseller.php">Reseller</a></li>
                                    <li><a href="/restream.php">Restream</a>
                                    <li><a href="/multiroom.php">Multiroom</a></li>
                                    <li><a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/service/line/new/101" target="_blank">24Hour</a></li>
                                    <li><a href="https://www.youtube.com/channel/UCJGO-yy54MwvfEVzqWnLXQw" target="_blank">Tutorial</a></li>
                                    
                                </ul>
                            </div>
                            <!-- Nav End -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header Area End -->

    <!-- Breadcrumb Area Start -->
    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.php"><i class="icon_house_alt"></i> Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Multiroom</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Area End -->

    <!-- Price Plan Area Start -->
    <section class="hami-price-plan-area mt-50">
        <div class="container">
            <div class="row">
                <!-- Section Heading -->
                <div class="col-12">
                    <div class="section-heading text-center">
                        <h1>IPTV Multiroom Plan</h1>
                        <p>You can finalize your purchase by choosing any of the following options then you can easily use one account on two devices as well as two different ip addresses.</p>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center">

                <!-- Single Price Plan -->
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="single-price-plan mb-100">
                        <!-- Title -->
                        <div class="price-plan-title">
                            <h4>1 Month</h4>
                            <p>2 Connection</p>
                        </div>
                        <!-- Value -->
                        <div class="price-plan-value">
                            <h2><span>€</span>25.00</h2>
                            <p>1 month</p>
                        </div>
                        <!-- Button -->
                        <a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/service/line/new/121" target="_blank" class="btn hami-btn w-100 mb-30">Get Started</a>
                        <!-- Description -->
                        <div class="price-plan-desc">
                            <p><i class="icon_check"></i> +12.000 Channels</p>
                            <p><i class="icon_check"></i> +40.000 VOD</p>
                            <p><i class="icon_check"></i> %99.99 Uptime</p>
                            <p><i class="icon_check"></i> Without Freezing</p>
                            <p><i class="icon_check"></i> Android,PC,SmartTv</p>
                            <p><i class="icon_check"></i> M3U MAG Enigma2</p>
                            <p><i class="icon_check"></i> 24/7 Support</p>
                        </div>
                        <!-- View All Feature Button -->
                    </div>
                </div>
                <!-- Single Price Plan -->
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="single-price-plan mb-100">
                        <!-- Title -->
                        <div class="price-plan-title">
                            <h4>3 Month</h4>
                            <p>2 Connection</p>
                        </div>
                        <!-- Value -->
                        <div class="price-plan-value">
                            <h2><span>€</span>35.00</h2>
                            <p>3 month</p>
                        </div>
                        <!-- Button -->
                        <a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/service/line/new/122" target="_blank" class="btn hami-btn w-100 mb-30">Get Started</a>
                        <!-- Description -->
                        <div class="price-plan-desc">
                            <p><i class="icon_check"></i> +12.000 Channels</p>
                            <p><i class="icon_check"></i> +40.000 VOD</p>
                            <p><i class="icon_check"></i> %99.99 Uptime</p>
                            <p><i class="icon_check"></i> Without Freezing</p>
                            <p><i class="icon_check"></i> Android,PC,SmartTv</p>
                            <p><i class="icon_check"></i> M3U MAG Enigma2</p>
                            <p><i class="icon_check"></i> 24/7 Support</p>
                        </div>
                        <!-- View All Feature Button -->
                    </div>
                </div>
                <!-- Single Price Plan -->
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="single-price-plan active mb-100">
                        <!-- Popular Tag -->
                        <div class="popular-tag">
                            <i class="icon_star"></i> Best Plan <i class="icon_star"></i>
                        </div>
                        <!-- Title -->
                        <div class="price-plan-title">
                            <h4>6 Month</h4>
                            <p>2 Connection</p>
                        </div>
                        <!-- Value -->
                        <div class="price-plan-value">
                            <h2><span>€</span>60.00</h2>
                            <p>6 month</p>
                        </div>
                        <!-- Button -->
                        <a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/service/line/new/123" target="_blank" class="btn hami-btn w-100 mb-30">Get Started</a>
                        <!-- Description -->
                        <div class="price-plan-desc">
                            <p><i class="icon_check"></i> +12.000 Channels</p>
                            <p><i class="icon_check"></i> +40.000 VOD</p>
                            <p><i class="icon_check"></i> %99.99 Uptime</p>
                            <p><i class="icon_check"></i> Without Freezing</p>
                            <p><i class="icon_check"></i> Android,PC,SmartTv</p>
                            <p><i class="icon_check"></i> M3U MAG Enigma2</p>
                            <p><i class="icon_check"></i> 24/7 Support</p>
                        </div>
                        <!-- View All Feature Button -->
                    </div>
                </div>

                <!-- Single Price Plan -->
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="single-price-plan mb-100">
                        <!-- Title -->
                        <div class="price-plan-title">
                            <h4>12 Month</h4>
                            <p>2 Connection</p>
                        </div>
                        <!-- Value -->
                        <div class="price-plan-value">
                            <h2><span>€</span>95.00</h2>
                            <p>12 month</p>
                        </div>
                        <!-- Button -->
                        <a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/service/line/new/124" target="_blank" class="btn hami-btn w-100 mb-30">Get Started</a>
                        <!-- Description -->
                        <div class="price-plan-desc">
                            <p><i class="icon_check"></i> +12.000 Channels</p>
                            <p><i class="icon_check"></i> +40.000 VOD</p>
                            <p><i class="icon_check"></i> %99.99 Uptime</p>
                            <p><i class="icon_check"></i> Without Freezing</p>
                            <p><i class="icon_check"></i> Android,PC,SmartTv</p>
                            <p><i class="icon_check"></i> M3U MAG Enigma2</p>
                            <p><i class="icon_check"></i> 24/7 Support</p>
                        </div>
                        <!-- View All Feature Button -->
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- Price Plan Area End -->

    <!-- Call To Action Area Start -->
    <section class="hami-call-to-action">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-md-6">
                    <div class="cta-thumbnail pr-3 mb-100">
                        <img src="/img/bg-img/3.png" alt="">
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="cta--content pl-3 mb-100">
                        <h2>Advantages of Multiroom</h2>
                        <p>Using the same account on two different devices as well as two different ip addresses. You can set up an account on a smart TV and set up the same account with the same features on your smartphone and enjoy watching channels.</p>
                        <!-- Button -->
                        <a href="https://client.<?php echo str_replace('www.', '', $_SERVER['HTTP_HOST']) ?>/register" target="_blank" class="btn hami-btn mt-50">Get Start Now!</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Call To Action Area End -->
    <!-- Support Area Start -->
    <section class="hami-support-area bg-White">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="support-text">
                        <h2>Need help? Call our award-winning support team 24/7: Chat online</h2>
                    </div>
                </div>
            </div>
        </div>

        <!-- Support Pattern -->
        <div class="support-pattern" style="background-image: url(/img/core-img/support-pattern.png);"></div>
    </section>
    <!-- Support Area End -->

    <!-- Call To Action Area Start -->
    <section class="hami-cta-area">
        <div class="container">
            <div class="cta-text">
                <h2>We Are Proud To Host More Than <span class="counter">800</span> Reseller and Over <span class="counter">50,000</span> Client</h2>
            </div>
        </div>
    </section>
    <!-- Call To Action Area End -->

    <!-- Footer Area Start -->
    <footer class="footer-area section-padding-80-0">

        <!-- Bottom Footer Area -->
        <div class="bottom-footer-area bg-gray">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-md-6">
                        <!-- Copywrite Text -->
                        <div class="copywrite-text">
                            <p><!-- Link back to Colorlib can't be removed. Website is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This WebSite is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="/" target="_blank">Tomas Arana</a>
<!-- Link back to Colorlib can't be removed. Website is licensed under CC BY 3.0. -->
</p>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <!-- Payment Methods -->
                        <div class="payments-methods d-flex align-items-center">
                            <p>Payments We Accept</p>
                            <i class="fa fa-cc-visa" aria-hidden="true"></i>
                            <i class="fa fa-cc-mastercard" aria-hidden="true"></i>
                            <i class="fa fa-cc-discover" aria-hidden="true"></i>
                            <i class="fa fa-cc-amex" aria-hidden="true"></i>
                            <i class="fa fa-cc-paypal" aria-hidden="true"></i>
                            <i class="fa fa-cc-stripe" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Area End -->

    <!-- **** All JS Files ***** -->
    <!-- jQuery 2.2.4 -->
    <script src="js/jquery.min.js"></script>
    <!-- Popper -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap -->
    <script src="js/bootstrap.min.js"></script>
    <!-- All Plugins -->
    <script src="js/hami.bundle.js"></script>
    <!-- Active -->
    <script src="js/default-assets/active.js"></script>

</body>

</html>