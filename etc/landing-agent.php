<?php

include_once('/var/www/inc/.php');


if( strstr($_SERVER['HTTP_USER_AGENT'], 'UptimeRobot') )
	die;

$res = wget( PORT_NODE.'/landing/insert/', [
		'timeout' => 2,
		'post' => [
			'domain' => trim($_SERVER['HTTP_HOST']),
			'request_uri' => trim($_SERVER['REQUEST_URI']),
			'user_ip' => trim( $_SERVER['HTTP_CF_CONNECTING_IP'] ? : $_SERVER['REMOTE_ADDR'] ),
			'user_agent' => trim($_SERVER['HTTP_USER_AGENT']),
			'user_referer' => trim( $_SERVER['HTTP_REFERER'] ? : '-'),
			'user_os' => trim( str_replace('"', '', $_SERVER['HTTP_SEC_CH_UA_PLATFORM'] ? : ($_SERVER['HTTP_SEC_CH_UA_MOBILE'] ? : '-') ) ),
			'user_country' => trim($_SERVER['HTTP_CF_IPCOUNTRY']),
		]
	]
);

