<?php

# 25 Dec 2023

/*

:: etc
 : timeout
 : user_agent
 : post

*/

function wget( $url, $etc=null ){

    $header['ssl'] = [ 'verify_peer'=>false, 'verify_peer_name'=>false ];
	
	if( array_key_exists('timeout', $etc) )
		$header['http']['timeout'] = $etc['timeout'];

	if( array_key_exists('user_agent', $etc) )
		$header['http']['header'].= 'User-Agent: '.$etc['user_agent']."\r\n";

	if( array_key_exists('post', $etc) ){
		$header['http']['method'] = 'POST';
		$header['http']['header'].= "Content-Type: application/x-www-form-urlencoded\r\n";
		$header['http']['content'] = http_build_query($etc['post']);
	}
	
	$res = file_get_contents($url, false, stream_context_create($header));
	
	if( substr($res, 0, 22) == "<br />\n<b>Warning</b>:" ){
		echo $res;
		return false;

	} else {
		return $res;
	}

}



